#ifndef __CUDA__TOOLS__HPP
#define __CUDA__TOOLS__HPP

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <stdexcept>

inline void checkCudaError(cudaError_t &err)
{
    if (err != cudaSuccess)
    {
        throw std::runtime_error(cudaGetErrorString(err));
    }
}

#endif
