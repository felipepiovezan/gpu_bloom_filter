#ifndef __FASTA_PARSER__HPP
#define __FASTA_PARSER__HPP

#include <vector>
#include <string>

namespace fasta_parser
{
std::vector<uint8_t> parse_into_byte_vector(const std::string& filename);
}

#endif
