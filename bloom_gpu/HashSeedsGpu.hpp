#ifndef __HASH_FUNCTOR_HPP__
#define __HASH_FUNCTOR_HPP__

#include <cuda.h>
#include <cuda_runtime_api.h>

#include "cuda_tools.hpp"

#include <vector>

class HashSeedsGpu
{
public:

    /** Constructor. This is entirely based on the Minia implementation.
     * \param[in] num_hash : number of hash functions to be used
     * \param[in] seed : some initialization code for defining the hash 
     * functions. */
    HashSeedsGpu (size_t num_hash, uint64_t user_seed=0) : num_hash_(num_hash),
        user_seed_(user_seed), generated_seeds_gpu_(nullptr), 
        generated_seeds_(NSEEDSBLOOM)
    {
        auto err = cudaMalloc(&generated_seeds_gpu_,
                NSEEDSBLOOM * sizeof(uint64_t));
        checkCudaError(err);

        generate_hash_seed ();
    }
    
    virtual ~HashSeedsGpu()
    {
        auto err = cudaFree(generated_seeds_gpu_);
        checkCudaError(err);
    }

    uint64_t* get_device_array()
    {
        return generated_seeds_gpu_;
    }

    std::vector<uint64_t>& get_host_array()
    {
        return generated_seeds_;
    }

private:

    /* */
    void generate_hash_seed ()
    {
        static const u_int64_t rbase[NSEEDSBLOOM] =
        {
            0xAAAAAAAA55555555ULL,  0x33333333CCCCCCCCULL,  
            0x6666666699999999ULL,  0xB5B5B5B54B4B4B4BULL,
            0xAA55AA5555335533ULL,  0x33CC33CCCC66CC66ULL,  
            0x6699669999B599B5ULL,  0xB54BB54B4BAA4BAAULL,
            0xAA33AA3355CC55CCULL,  0x33663366CC99CC99ULL
        };

        for (size_t i=0; i<NSEEDSBLOOM; ++i)  
        { 
            generated_seeds_[i] = rbase[i];  
        }
        
        for (size_t i=0; i<NSEEDSBLOOM; ++i)  
        { 
            generated_seeds_[i] = generated_seeds_[i] * 
                generated_seeds_[(i+3) % NSEEDSBLOOM] + user_seed_;
        }
        
        auto err = cudaMemcpy(generated_seeds_gpu_, generated_seeds_.data(),
                NSEEDSBLOOM * sizeof(uint64_t), cudaMemcpyHostToDevice);
        checkCudaError(err);
    }

    static constexpr size_t NSEEDSBLOOM = 10;
    
    size_t num_hash_;
    uint64_t user_seed_;
    uint64_t *generated_seeds_gpu_;
    std::vector<uint64_t> generated_seeds_;
};

#endif
