#ifndef __BLOOM_GPU__HPP
#define __BLOOM_GPU__HPP

#include <cuda.h>
#include <cuda_runtime_api.h>
#include "cuda_tools.hpp"

#include <vector>
#include <bitset>

#include "HashSeedsGpu.hpp"
#include "bloom_operations.hpp"

/** \brief Bloom interface
 *
 * We define a Bloom filter has a container (something that tells whether an item is here or not) and
 * a bag (something we can put items into).
 *
 * This interface has a template argument corresponding to the type of items to be inserted in the filter.
 * Note that implementations should provide hash functions supporting the template type.
 *
 * As expected, there is no Iterable interface here because it is not possible to enumerate the items
 * inserted in a Bloom filter only with the Bloom filter information.
 */
template <typename Item> class BloomGpu
{
public:

    BloomGpu (uint64_t bloom_size, size_t num_hash = 4) : 
        gpu_bloom_array_(nullptr), gpu_bloom_array_size_((bloom_size + 7) / 8),
        num_hash_(num_hash), hashes_(num_hash_, 17)
    {
        void *mem = nullptr;
        auto err = cudaMalloc(&mem, gpu_bloom_array_size_);
        gpu_bloom_array_ = static_cast<uint8_t*>(mem);
        checkCudaError(err);

        err = cudaMemset(gpu_bloom_array_, 0, gpu_bloom_array_size_);
        checkCudaError(err);
    }

    virtual ~BloomGpu() 
    {
        auto err = cudaFree(gpu_bloom_array_);
        checkCudaError(err);
    }

    /******************************************************************
     * *************** GPU_RELATED_METHODS ****************************/

    std::vector<uint8_t> get_host_array() const
    {
        std::vector<uint8_t> ans(gpu_bloom_array_size_);
        auto ptr = ans.data();

        auto err = cudaMemcpy(ptr, gpu_bloom_array_, gpu_bloom_array_size_,
                cudaMemcpyDeviceToHost);
        checkCudaError(err);

        return ans;
    }

    uint8_t* get_device_array()
    {
        return gpu_bloom_array_;
    }

    
    /******************************************************************
     * *************** MINIA_DEFINED_METHODS *************************/

    void insert (const Item& item)
    {
        Item *device_items = nullptr;
        auto err = cudaMalloc(&device_items, sizeof(Item));
        checkCudaError(err);
        err = cudaMemcpy(device_items, &item, sizeof(Item), 
                cudaMemcpyHostToDevice);
        checkCudaError(err);

        dim3 threads(num_hash_,1,1), blocks(1, 1, 1);
        bloom_insert<<<blocks, threads>>>(device_items, gpu_bloom_array_,
                gpu_bloom_array_size_, hashes_.get_device_array());

        err = cudaFree(device_items);
        checkCudaError(err);
    }

    void insert(const Item *items_ptr, uint64_t num_items)
    {
        Item *device_items = nullptr;
        auto err = cudaMalloc(&device_items, sizeof(Item) * num_items);
        checkCudaError(err);
        err = cudaMemcpy(device_items, items_ptr, sizeof(Item) * num_items, 
                cudaMemcpyHostToDevice);
        checkCudaError(err);

        dim3 threads(num_hash_,1,1), blocks(num_items, 1, 1);
        bloom_insert<<<blocks, threads>>>(device_items, gpu_bloom_array_,
                gpu_bloom_array_size_, hashes_.get_device_array());

        err = cudaFree(device_items);
        checkCudaError(err);
    }

    bool contains (const Item& item)
    {
        Item *device_items = nullptr;
        auto err = cudaMalloc(&device_items, sizeof(Item));
        checkCudaError(err);
        err = cudaMemcpy(device_items, &item, sizeof(Item), 
                cudaMemcpyHostToDevice);
        checkCudaError(err);
 
        bool *ans = nullptr;
        err = cudaMalloc(&ans, sizeof(bool));
        checkCudaError(err);

        dim3 threads(num_hash_,1,1), blocks(1, 1, 1);
        bloom_contains<<<blocks, threads>>>(device_items, gpu_bloom_array_,
                gpu_bloom_array_size_, hashes_.get_device_array(), ans);

        bool ret;
        err = cudaMemcpy(&ret, ans, sizeof(bool), 
                cudaMemcpyDeviceToHost);
        checkCudaError(err);

        err = cudaFree(ans);
        checkCudaError(err);
        err = cudaFree(device_items);
        checkCudaError(err);

        return ret;
    }

    std::vector<uint8_t> contains (const Item *items_ptr, uint64_t num_items)
    {
        Item *device_items = nullptr;
        auto err = cudaMalloc(&device_items, sizeof(Item) * num_items);
        checkCudaError(err);
        err = cudaMemcpy(device_items, items_ptr, sizeof(Item) * num_items,
                cudaMemcpyHostToDevice);
        checkCudaError(err);
 
        bool *ans = nullptr;
        err = cudaMalloc(&ans, sizeof(bool) * num_items);
        checkCudaError(err);

        dim3 threads(num_hash_,1,1), blocks(num_items, 1, 1);
        bloom_contains<<<blocks, threads>>>(device_items, gpu_bloom_array_,
                gpu_bloom_array_size_, hashes_.get_device_array(), ans);

        std::vector<uint8_t> ret(num_items);

        err = cudaMemcpy(ret.data(), ans, sizeof(bool) * num_items, 
                cudaMemcpyDeviceToHost);
        checkCudaError(err);

        err = cudaFree(ans);
        checkCudaError(err);
        err = cudaFree(device_items);
        checkCudaError(err);

        return ret;
    }

    /** Get the raw bit set of the Bloom filter.
     * \return Bloom filter's bit set. */
    virtual u_int8_t*& getArray    ()
    {
        return gpu_bloom_array_;
    };

    /** Get the size of the Bloom filter (in bytes).
     * \return the size of the bit set of the Bloom filter */
    virtual u_int64_t  getSize     ()
    {
        return gpu_bloom_array_size_;
    };

    /** Get the size of the Bloom filter (in bits).
     * \return the size of the bit set of the Bloom filter */
    virtual u_int64_t  getBitSize  ()
    {
        return 8 * getSize();
    };

    /** Get the number of hash functions used for the Bloom filter.
     * \return the number of hash functions. */
    virtual size_t     getNbHash   () const
    {
        return num_hash_;
    };

    /** Tells whether the 4 neighbors of the given item are in the Bloom filter.
     * The 4 neighbors are computed from the given item by adding successively
     * nucleotides 'A', 'C', 'T' and 'G'
     * Note: some implementation may not provide this service.
     * \param[in] item : starting item from which neighbors are computed.
     * \param[in] right : if true, successors are computed, otherwise predecessors are computed.
     * \return a bitset with a boolean for the 'contains' status of each neighbor
     */
	virtual std::bitset<4> contains4 (const Item& item, bool right)
    {
        return std::bitset<4>();
    }

    /** Tells whether the 8 neighbors of the given item are in the Bloom filter.
     * A default implementation may be two calls to contains4 with right==true and
     * right==left and by concatenating the two bitsets.
     * Note: some implementation may not provide this service.
     * \param[in] item : starting item from which neighbors are computed.
     * \return a bitset with a boolean for the 'contains' status of each neighbor
     */
    virtual std::bitset<8> contains8 (const Item& item)
    {
        return std::bitset<8>();
    }

    /** Get the name of the implementation class.
     * \return the class name. */
    virtual std::string  getName()
    {
        return "BloomGpu";
    }

    /** Return the number of 1's in the Bloom (nibble by nibble)
     * \return the weight of the Bloom filter */
    virtual unsigned long  weight ()
    {
        return 0;
    }

private:
    uint8_t *gpu_bloom_array_;
    uint64_t gpu_bloom_array_size_;

    size_t num_hash_;
    HashSeedsGpu hashes_;
};

#endif
