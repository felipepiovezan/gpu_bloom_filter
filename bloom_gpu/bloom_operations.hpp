#ifndef __BLOOM_OPERATIONS_HPP__
#define __BLOOM_OPERATIONS_HPP__

#include <cuda.h>
#include <cuda_runtime_api.h>

#include <stdint.h>
#include <array>

template <uint64_t N> using Element = uint64_t[N];

    /** Helper device function that, given an array, sets the bit at the
     * desired position.
     * Note: no bound checking is performed.
     * Note: not thread safe. Two threads attempting to set a bit contained in
     * the same byte is undefined behavior.
     * \param[in] ptr : the array representing the bitset.
     * \param[in] pos : the position of the bit to be set.
     */
__device__ inline void set_bit(uint8_t *ptr, uint64_t pos)
{
    uint8_t byte_pos = pos % 8;
    pos = pos/8;
    constexpr uint8_t one = 1;

    ptr[pos] |= one << byte_pos;
}
    
    /** Wrapper around set_bit so that it can be called as a kernel.
      * Note: should be called with dim 1,1,1.
     */
__global__ inline void set_bit_kernel(uint8_t *ptr, uint64_t pos)
{
    set_bit(ptr, pos);
}

    /** Same as set_bit but thread safe. 
     */
__device__ inline void set_bit_atomic(uint8_t *ptr, uint64_t pos)
{
    uint32_t byte_pos = pos % 32;
    pos = pos/32;
    constexpr uint32_t one = 1;

    auto ptr32 = reinterpret_cast<uint32_t*>(ptr);
    atomicOr(&ptr32[pos], one << byte_pos);
}

__global__ inline void set_bit_atomic_kernel_single(uint8_t *ptr, uint64_t pos)
{
    set_bit_atomic(ptr, pos);
}
    /** Wrapper around set_bit_atomic so that it can be called as a kernel.
      * Note: should be called with dim (x,1,1), (y,1,1).
     */
__global__ inline void set_bit_atomic_kernel(uint8_t *ptr, uint64_t *pos, uint64_t pos_size)
{
    uint64_t i = blockIdx.x *blockDim.x + threadIdx.x;
    
    if (i < pos_size)
    {
        set_bit_atomic(ptr, pos[i]);
    }
}


__device__ inline uint64_t hash64_device (uint64_t value, uint64_t seed)
{
    uint64_t hash = seed;
    hash ^= (hash <<  7) ^  value * (hash >> 3) ^ (~((hash << 11) + (value ^ (hash >> 5))));
    hash = (~hash) + (hash << 21); // hash = (hash << 21) - hash - 1;
    hash = hash ^ (hash >> 24);
    hash = (hash + (hash << 3)) + (hash << 8); // hash * 265
    hash = hash ^ (hash >> 14);
    hash = (hash + (hash << 2)) + (hash << 4); // hash * 21
    hash = hash ^ (hash >> 28);
    hash = hash + (hash << 31);

    return hash;
}


__device__ inline uint64_t compute_hash(uint64_t value, uint64_t key)
{
    return hash64_device(value, key);
}

template <uint64_t N>
__device__ inline uint64_t compute_hash(const Element<N> &value, uint64_t seed)
{
    uint64_t ans = 0;
    
    for (uint64_t i = 0; i < N; i++)
    {
        ans ^= hash64_device(value[i], seed);
    }

    return ans;
}

template <uint64_t N>
__global__ inline void compute_hash_kernel(Element<N> *values, uint64_t values_size,
        uint64_t seed, uint64_t *ans)
{
    uint64_t i = blockIdx.x *blockDim.x + threadIdx.x;

    if (i < values_size)
    {
        ans[i] = compute_hash(values[i], seed);
    }
}

    /** Kernel that tests whether some elements are in the bloom.
      * Note: should be called with dim (y,1,1), (x,1,1),
      * where y is the number of elements and x is the number of
      * hash functions.
     */
template <uint64_t N>
__global__ inline void bloom_insert(Element<N> *elements, 
        uint8_t *bloom, uint64_t bloom_size, uint64_t *hash_seeds)
{
    uint64_t hash_idx= threadIdx.x;
    uint64_t element_idx = blockIdx.x;
    
    uint64_t bit_pos = compute_hash(elements[element_idx], hash_seeds[hash_idx])
                        % (8 * bloom_size);
    set_bit_atomic(bloom, bit_pos);
}

    /** Kernel that tests whether some elements are in the bloom.
      * Note: should be called with dim (y,1,1), (x,1,1),
      * where y is the number of elements and x is the number of
      * hash functions.
     */
template <uint64_t N>
__global__ inline void bloom_contains(Element<N> *elements,
        uint8_t *bloom, uint64_t bloom_size, 
        uint64_t *hash_seeds, bool *ans)
{
    __shared__ uint32_t num_bits_set;
    num_bits_set = 0;

    __syncthreads();

    uint64_t hash_idx= threadIdx.x;
    uint64_t element_idx = blockIdx.x;
    uint64_t pos = compute_hash(elements[element_idx], hash_seeds[hash_idx])
                    % (8 * bloom_size);
    
    uint8_t byte_pos = pos % 8;
    pos = pos/8;
    constexpr uint8_t one = 1;

    uint32_t bit = (bloom[pos] && (one << byte_pos)) != 0;
    atomicAdd(&num_bits_set, bit);

    if (threadIdx.x == 0)
    {
        uint64_t num_hashes = blockDim.x;
        ans[element_idx] = num_bits_set == num_hashes;
    }
}


#endif
