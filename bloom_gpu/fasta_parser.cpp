#include "fasta_parser.hpp"
#include <cctype>
#include <fstream>
#include <string>
#include <iostream>

namespace fasta_parser
{

std::vector<uint8_t> parse_into_byte_vector(const std::string& filename)
{
    std::ifstream file(filename);
    
    if (!file.is_open())
    {
        throw std::exception();
    }

    std::vector<uint8_t> ans;
    std::string line;
    //ignore first line of the input
    std::getline(file, line);

    uint8_t byte = 0;
    uint32_t count = 0;
    while (std::getline(file, line))
    {       
        for (auto x : line)
        {
            x = toupper(x);

            switch (x)
            {
                case 'A':
                    byte <<= 2;
                    count++;
                    byte |= 0;
                    break;
                case 'C':
                    byte <<= 2;
                    count++;
                    byte |= 1;
                    break;
                case 'G':
                    byte <<= 2;
                    count++;
                    byte |= 2;
                    break;
                case 'T':
                    byte <<= 2;
                    count++;
                    byte |= 3;
                    break;
            }

            if (count == 4)
            {
                ans.push_back(byte);
                byte = 0;
                count = 0;
            }
        }
    }

    if (count)
    {
        ans.push_back(byte);
    }

    std::cout << "Parser finished parsing " << filename << ". Nucleotide count = " << 
        4 * ans.size() << std::endl;
    return ans;
}


}


