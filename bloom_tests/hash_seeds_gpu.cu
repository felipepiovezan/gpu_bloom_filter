#include "HashSeedsGpu.hpp"
#include "bloom_operations.hpp"

#include <cuda.h>
#include <cuda_runtime_api.h>

#include "gtest/gtest.h"

namespace
{
    uint64_t hash64(uint64_t value, uint64_t seed)
    {
        uint64_t hash = seed;
        hash ^= (hash <<  7) ^  value * (hash >> 3) ^ (~((hash << 11) + (value ^ (hash >> 5))));
        hash = (~hash) + (hash << 21); // hash = (hash << 21) - hash - 1;
        hash = hash ^ (hash >> 24);
        hash = (hash + (hash << 3)) + (hash << 8); // hash * 265
        hash = hash ^ (hash >> 14);
        hash = (hash + (hash << 2)) + (hash << 4); // hash * 21
        hash = hash ^ (hash >> 28);
        hash = hash + (hash << 31);

        return hash;
    }
}

TEST(HashSeedsGpu, hashes_are_computed_properly)
{
    auto num_hashes = 10;
    HashSeedsGpu hashes(num_hashes, 17);
    constexpr uint64_t input_size = 4;
    
    Element<3> inputs[input_size] = 
                                       {{123123, 91284671, 192478},
                                        {1023978, 102934, 1029381},
                                        {1023978, 102934, 1029381},
                                        {999999, 219389, 1222222}}; 
    Element<3> *inputs_device = nullptr;
    uint64_t *ans_device = nullptr;
    uint64_t ans_host[input_size];

    auto err = cudaMalloc(&inputs_device,
           sizeof(inputs));
    checkCudaError(err);
    err = cudaMalloc(&ans_device,
           input_size * sizeof(uint64_t));
    checkCudaError(err);

    err = cudaMemcpy(inputs_device, inputs,
            sizeof(inputs), cudaMemcpyHostToDevice);
    checkCudaError(err);

    for (auto seed : hashes.get_host_array())
    {
        compute_hash_kernel<<<input_size, 1, 1>>>(inputs_device, input_size, seed, ans_device);
        err = cudaMemcpy(ans_host, ans_device,
            input_size * sizeof(uint64_t), cudaMemcpyDeviceToHost);
        checkCudaError(err);

        for (uint64_t i = 0; i < input_size; i++)
        {
            auto expected =  hash64(inputs[i][0], seed) ^
                             hash64(inputs[i][1], seed) ^ 
                             hash64(inputs[i][2], seed);
            ASSERT_EQ(ans_host[i], expected);
        }
    }

    cudaFree(inputs_device);
    cudaFree(ans_device);
}
