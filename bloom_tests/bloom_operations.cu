#include "BloomGpu.hpp"
#include "bloom_operations.hpp"
#include "gtest/gtest.h"

#include <random>
#include <algorithm>
#include <bitset>
#include <chrono>

TEST(bloom_operation, set_bit_works_correctly_on_non_concurrent_sets)
{
    BloomGpu<int> bloom(2048);
    auto gpu_arr = bloom.get_device_array();
    set_bit_kernel<<<1,1,1>>>(gpu_arr, 0);

    auto host_arr = bloom.get_host_array();
    ASSERT_EQ(host_arr[0], 1);
    ASSERT_EQ(host_arr[1], 0);

    set_bit_kernel<<<1,1,1>>>(gpu_arr, 8);
    host_arr = bloom.get_host_array();
    ASSERT_EQ(host_arr[0], 1);
    ASSERT_EQ(host_arr[1], 1);

    set_bit_kernel<<<1,1,1>>>(gpu_arr, 1);
    host_arr = bloom.get_host_array();
    ASSERT_EQ(host_arr[0], 3);
    ASSERT_EQ(host_arr[1], 1);
}

TEST(bloom_operation, set_bit_atomic_works_correctly_on_non_concurrent_sets)
{
    BloomGpu<int> bloom(2048);
    auto gpu_arr = bloom.get_device_array();
    set_bit_atomic_kernel_single<<<1,1,1>>>(gpu_arr, 0);

    auto host_arr = bloom.get_host_array();
    ASSERT_EQ(host_arr[0], 1);
    ASSERT_EQ(host_arr[1], 0);

    set_bit_atomic_kernel_single<<<1,1,1>>>(gpu_arr, 8);
    host_arr = bloom.get_host_array();
    ASSERT_EQ(host_arr[0], 1);
    ASSERT_EQ(host_arr[1], 1);

    set_bit_atomic_kernel_single<<<1,1,1>>>(gpu_arr, 1);
    host_arr = bloom.get_host_array();
    ASSERT_EQ(host_arr[0], 3);
    ASSERT_EQ(host_arr[1], 1);
}

TEST(bloom_operation, set_bit_atomic_works_correctly)
{
    uint64_t size = 1024ull*1024*10;
    uint64_t num_bits_to_set = (size)/10;

    uint64_t seed = 141;
    std::mt19937_64 gen(seed);
    std::uniform_int_distribution<uint64_t> dist(0, size-1);
    auto rand = [&](){return dist(gen);};

    std::vector<uint64_t> bits_to_set_host;
    for (uint64_t i = 0; i < num_bits_to_set; i++)
    {
        bits_to_set_host.push_back(rand());
    }

    uint64_t *bits_to_set_device = nullptr;
    auto err = cudaMalloc(&bits_to_set_device,
           num_bits_to_set * sizeof(uint64_t));
    checkCudaError(err);
    err = cudaMemcpy(bits_to_set_device, bits_to_set_host.data(), 
            num_bits_to_set * sizeof(uint64_t), cudaMemcpyHostToDevice);
    checkCudaError(err);
    BloomGpu<int> bloom(size);
    
    auto gpu_arr = bloom.get_device_array();
    dim3 threads(1024,1,1), blocks((size+1023)/1024, 1, 1);

    auto start = std::chrono::high_resolution_clock::now();
    set_bit_atomic_kernel<<<blocks, threads>>>(
            gpu_arr, bits_to_set_device, num_bits_to_set);
    cudaDeviceSynchronize();
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Kernel Elapsed time: " << elapsed.count() << " s\n";


    start = std::chrono::high_resolution_clock::now();
    auto host_arr = bloom.get_host_array();
    uint64_t total = 0;
    std::sort(bits_to_set_host.begin(), bits_to_set_host.end());
    
    for (auto x : host_arr)
    {
        std::bitset<8> bits(x);
        for (int i = 0; i < 8; i++)
        {
            bool should_set = std::binary_search(
                    bits_to_set_host.begin(), bits_to_set_host.end(),
                    total);
            ASSERT_EQ(bits[i], should_set) << "failed at:  " << total << std::endl;
            total++;
        }
    }
    finish = std::chrono::high_resolution_clock::now();
    elapsed = finish - start;
    std::cout << "Asserting Elapsed time: " << elapsed.count() << " s\n";
}
