#include <random>
#include <memory>

#include "BloomGpu.hpp"
#include "gtest/gtest.h" 

TEST(bloom_gpu, bloom_filter_is_zero_initialized)
{
    BloomGpu<uint64_t> bloom(113223);
    auto vec = bloom.get_host_array();

    for (auto x : vec)
    {
        ASSERT_EQ(x, 0);
    }
}

TEST(bloom_gpu, bloom_filter_is_created_with_the_right_size)
{
    {
        BloomGpu<uint64_t> bloom(1);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 1); 
    }
    {
        BloomGpu<uint64_t> bloom(2);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 1); 
    }
    {
        BloomGpu<uint64_t> bloom(7);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 1); 
    }
    {
        BloomGpu<uint64_t> bloom(8);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 1); 
    }
    {
        BloomGpu<uint64_t> bloom(9);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 2); 
    }
    {
        BloomGpu<uint64_t> bloom(18);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 3); 
    }
    {
        BloomGpu<uint64_t> bloom(1024);
        auto vec = bloom.get_host_array();
        ASSERT_EQ(vec.size(), 128); 
    }
}

TEST(bloom_gpu, bloom_filter_inserts_and_contains_correctly_N_is_1)
{
    uint64_t seed = 141;
    std::mt19937_64 rand(seed);
    
    {
        uint64_t bloom_size = 1024*1024*32;
        BloomGpu<Element<1>> bloom(bloom_size);

        for (uint64_t i = 0; i < 1024; i++)
        {
            uint64_t element = rand();
            ASSERT_FALSE(bloom.contains({element}));
            bloom.insert({element});
            ASSERT_TRUE(bloom.contains({element}));
        }
    }
    {
        uint64_t bloom_size = 1024*1024*1024;
        BloomGpu<Element<1>> bloom(bloom_size);

        for (uint64_t i = 0; i < 1024*32; i++)
        {
            uint64_t element = rand();
            ASSERT_FALSE(bloom.contains({element}));
            bloom.insert({element});
            ASSERT_TRUE(bloom.contains({element}));
        }
    }
}

TEST(bloom_gpu, bloom_filter_inserts_and_contains_correctly_N_is_3)
{
    uint64_t seed = 141123;
    std::mt19937_64 rand(seed);
    
    {
        uint64_t bloom_size = 1024*1024*32;
        BloomGpu<Element<3>> bloom(bloom_size);

        for (uint64_t i = 0; i < 1024; i++)
        {
            Element<3> element = {rand(), rand(), rand()};
            ASSERT_FALSE(bloom.contains(element));
            bloom.insert(element);
            ASSERT_TRUE(bloom.contains(element));
        }
    }   
    {
        uint64_t bloom_size = 1024*1024*1024;
        BloomGpu<Element<3>> bloom(bloom_size);

        for (uint64_t i = 0; i < 1024*32; i++)
        {
            Element<3> element = {rand(), rand(), rand()};
            ASSERT_FALSE(bloom.contains(element));
            bloom.insert(element);
            ASSERT_TRUE(bloom.contains(element));
        }
    }
}

TEST(bloom_gpu, bloom_filter_inserts_and_contains_multiples_correctly_N_is_3)
{
    uint64_t seed = 141123;
    std::mt19937_64 rand(seed);
    uint64_t num_elements = 1024*1024;
    uint64_t bloom_size = 1024*1024*1024;
    BloomGpu<Element<3>> bloom(bloom_size);

    std::unique_ptr<Element<3>[]> input(new Element<3>[num_elements]);

    for (uint64_t i = 0; i < num_elements; i++)
    {
       input[i][0] = rand();
       input[i][1] = rand();
       input[i][2] = rand();
    }

    auto ans = bloom.contains(input.get(), num_elements);

    for (auto x : ans)
    {
       ASSERT_FALSE(x);
    }

    bloom.insert(input.get(), num_elements);
    ans = bloom.contains(input.get(), num_elements);

    for (auto x : ans)
    {
       ASSERT_TRUE(x);
    }
}
