#include <random>
#include <memory>
#include <chrono>

#include "BloomGpu.hpp"
#include "fasta_parser.hpp"

#include "gtest/gtest.h" 

TEST(speed_test, contains)
{
    uint64_t seed = 141123;
    std::mt19937_64 rand(seed);
    uint64_t bloom_size = 1024*1024*1024;

    uint64_t A = 0;
    uint64_t C = 1;
    uint64_t T = 2;
    uint64_t G = 3;
    
    for (uint64_t exp = 10; exp <= 29; exp++)
    {
        uint64_t num_elements = (1ull << exp);
        BloomGpu<Element<1>> bloom(bloom_size);
        
        auto start = std::chrono::high_resolution_clock::now();

        std::unique_ptr<Element<1>[]> input(new Element<1>[num_elements]);

        for (uint64_t i = 0; i < num_elements/4; i++)
        {
            uint64_t kmer = rand();
            input[4 * i + 0][0] = (kmer << 2) | A;
            input[4 * i + 1][0] = (kmer << 2) | C;
            input[4 * i + 2][0] = (kmer << 2) | T;
            input[4 * i + 3][0] = (kmer << 2) | G;
        }

        auto ans = bloom.contains(input.get(), num_elements);
    
        auto finish = std::chrono::high_resolution_clock::now();
        auto elapsed = finish - start;

        for (auto x : ans)
        {
           ASSERT_FALSE(x);
        }

        std::cout << "TEST::contains::2^" << exp << ":: time = " << 
            elapsed.count()/1000000 << "ms" << std::endl;
    }
}

TEST(speed_test, insert)
{
    uint64_t seed = 141123;
    std::mt19937_64 rand(seed);
    uint64_t bloom_size = 1024*1024*1024;

    for (uint64_t exp = 10; exp <= 29; exp++)
    {
        uint64_t num_elements = (1ull << exp);
        BloomGpu<Element<1>> bloom(bloom_size);
        
        auto start = std::chrono::high_resolution_clock::now();

        std::unique_ptr<Element<1>[]> input(new Element<1>[num_elements]);

        for (uint64_t i = 0; i < num_elements; i++)
        {
            uint64_t kmer = rand();
            input[i][0] = kmer;
        }

        bloom.insert(input.get(), num_elements);
    
        auto finish = std::chrono::high_resolution_clock::now();
        auto elapsed = finish - start;

        std::cout << "TEST::insert::2^" << exp << ":: time = " << 
            elapsed.count()/1000000 << "ms" << std::endl;
    }
}

TEST(speed_test, chr1)
{
    auto chr1 = fasta_parser::parse_into_byte_vector("bloom_tests/chr1.fa");
    auto chr1_64 = reinterpret_cast<uint64_t*>(chr1.data());
    
    uint64_t num_elements = chr1.size() / 8;

    uint64_t bloom_size = 1024*1024*1024;
    BloomGpu<Element<1>> bloom(bloom_size);

    std::unique_ptr<Element<1>[]> input_unique(new Element<1>[num_elements]);
    auto input = input_unique.get();

    for (uint64_t i = 0 ; i < num_elements; i++)
    {
        input[i][0] = chr1_64[i];
    }

    auto start = std::chrono::high_resolution_clock::now();
    
    bloom.insert(input, num_elements);
    
    auto finish = std::chrono::high_resolution_clock::now();
    auto elapsed = finish - start;

    std::cout << "TESTchr1::insert::" << num_elements << " 32-mers:: time = " << 
        elapsed.count()/1000000 << "ms" << std::endl;

    {
        uint64_t A = 0;
        uint64_t C = 1;
        uint64_t T = 2;
        uint64_t G = 3;

        std::unique_ptr<Element<1>[]> input_extended(new Element<1>[4*num_elements]);

        auto start = std::chrono::high_resolution_clock::now();

        for (uint64_t i = 0; i < num_elements; i++)
        {
            uint64_t kmer = input[i][0];
            std::cout << kmer << std::endl;
            input_extended[4 * i + 0][0] = (kmer << 2) | A;
            input_extended[4 * i + 1][0] = (kmer << 2) | C;
            input_extended[4 * i + 2][0] = (kmer << 2) | T;
            input_extended[4 * i + 3][0] = (kmer << 2) | G;
        }

        auto ans = bloom.contains(input_extended.get(), num_elements * 4);

        auto finish = std::chrono::high_resolution_clock::now();
        auto elapsed = finish - start;

        uint64_t dont_optimze = 0;
        for (auto x : ans)
        {
            if (x) dont_optimze ++;
        }

        std::cout << "TESTchr1::contains::" << num_elements << ":: time = " << 
            elapsed.count()/1000000 << "ms" << " " << dont_optimze 
            <<std::endl;
    }

}
